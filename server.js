const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 3001;

app.use(cors());
app.use(express.json());
app.use('/task', require('./routes/task'));
app.use('/user', require('./routes/user'));

mongoose.connect(process.env.MONGODB_URL, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
}).then(() => {
    console.log('Database Connected!');
}).catch((err) => {
    console.log(err);
})

app.listen(port, () => {
    console.log(`Server is running on ${port}`);
})